Ruby lib for easily defining solutions for non-deterministic problems caused by interdependent "black-box" components

Sorry ...

- not ready for production yet
- not documented
- tests not published yet

License: MIT


Hints for brainstorming:

If the problem has multiple causes, either:
- make the fix a multi-step achievement (like "redoing with diagnostics enabled")
- make the error specific enough to reveal the root cause


Copyright (c) Cezary Baginski 2015
