#!/usr/bin/env ruby

require 'logger'
require 'open3'
require 'set'
require 'pathname'

require 'unravel/session'

# TODO: don't allow replacing the error

module Unravel
  def self.logger
    @logger ||= Logger.new(STDOUT).tap do |logger|
      logger.level = (ENV['UNRAVEL_DEBUG']||"")[/1|true/] ? :debug : :info
      logger.formatter = proc do |severity, _datetime, _progname, msg|
        "#{severity}: #{msg}\n"
      end
    end
  end

  def self.logger=(logger)
    @logger = logger
  end

  class HumanInterventionNeeded < RuntimeError; end
  class NoKnownRootCause < HumanInterventionNeeded; end
  class OutOfAttempts < HumanInterventionNeeded; end
  class SameCauseReoccurringCause < HumanInterventionNeeded; end
end
