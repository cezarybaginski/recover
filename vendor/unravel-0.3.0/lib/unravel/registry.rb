require 'unravel/registry/sane_map'

module Unravel
  class Registry
    attr_reader :achievements, :root_cause_for, :fix_for, :error_matcher_for

    # TODO: not tested specifically
    class Achievements < SaneMap
      def []=(key, value)
        super(key, value)
      end

      def map
        @values.values.map do |item|
          yield item
        end
      end
    end

    def initialize
      @achievements = Achievements.new #  achievement_name => block
      # @error_context_for = Contexts.new # achievement_name => error_context

      @root_cause_for = SaneMap.new # error_name => cause_name
      @error_matcher_for = SaneMap.new # error_name => regexp

      @fix_for = SaneMap.new # cause_name => achievement_name
    end

    def detect_problems
      report = {}

      report['Unused'] = fix_for.keys - root_cause_for.values
      report['Unhandled'] = root_cause_for.values - fix_for.keys

      errors = achievements.map do |achievement|
        achievement.error_context.values
      end.flatten(2)

      report['Unknown contexts'] = errors - root_cause_for.keys
      report['Unused errors'] = root_cause_for.keys - errors
      report
    end
  end
end

require 'unravel/achievement'
