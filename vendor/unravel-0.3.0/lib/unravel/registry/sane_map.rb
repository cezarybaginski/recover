module Unravel
  class Registry
    class SaneMap
      class Error < RuntimeError
        class NoSuchKey < Error
        end

        class KeyExists < Error
        end
      end

      def initialize
        @values = {}
      end

      def [](name)
        raise Error::NoSuchKey, "No key: #{name.inspect}" unless @values.key?(name)
        @values[name]
      end

      def []=(name, value)
        raise Error::KeyExists, "Key: #{name.inspect} already exists" if @values.key?(name)
        @values[name] = value
      end

      def key?(name)
        @values.key?(name)
      end

      def keys
        @values.keys
      end

      def values
        @values.values
      end
    end
  end
end
