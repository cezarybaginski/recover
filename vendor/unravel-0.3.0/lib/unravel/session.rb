require 'unravel/session/default_config'
require 'unravel/session/api'
require 'English'

require 'unravel/registry'

module Unravel
  class Session
    class History
      class Entry
        attr_reader :prev_causes
        attr_reader :session

        def initialize(session, achievement)
          @session = session
          @achievement = achievement
          @prev_causes = Set.new
          @max_retries = session.config.max_retries
          @fixable_retries_left = @max_retries
        end

        def retries_left
          @fixable_retries_left
        end

        def max_retries
          @max_retries
        end

        def log_start
          Unravel.logger.info(indent + "Achieving #{name.inspect} (try: #{max_retries - retries_left + 1}/#{max_retries})")
        end

        def log_done
          Unravel.logger.info(indent + "Achieved #{name.inspect} (try: #{max_retries - retries_left + 1}/#{max_retries})")
        end

        def log_failure(error)
          Unravel.logger.info(indent + "Achieving #{name.inspect} failed. Symptom: #{error.symptom.inspect}")
        end

        def log_cause(cause)
          Unravel.logger.info(indent + "Investigating failed achievement #{name.inspect}. Cause: #{cause.inspect}")
        end

        def log_fix(fix_name)
          Unravel.logger.info(indent + "Unimpeding achievement #{name}. Fix: #{fix_name.inspect}")
        end

        def log_unknown_exception(ex)
          Unravel.logger.error(indent + "Achieving #{name.inspect} failed: unrecognized exception: #{ex.inspect}")
        end

        def assure_cause_has_not_occured(cause, unique_context)
          unique_cause = [cause, unique_context]
          return prev_causes << unique_cause unless prev_causes.include? unique_cause

          raise SameCauseReoccurringCause, "Cannot achieve #{name.inspect} because root cause #{cause.to_s.inspect} for error #{unique_context.inspect} still isn't fixed (the root causes's symptoms reoccurred)"
        end

        def assure_retires_available
          @fixable_retries_left -= 1
          return if retries_left > 0
          raise OutOfAttempts, "Achieving #{name.inspect} took too many attempts. Verify that errors are correctly matched and that the fixes are deterministic"
        end

        def end_call
        end

        def name
          @achievement.name
        end

        def indent
          session.indent
        end
      end
    end

    attr_reader :registry
    attr_reader :config
    attr_reader :indent

    include Unravel::Session::Api

    def initialize(config = DefaultConfig.new)
      @config = config
      @registry = Registry.new
      @history = {}
      @indent = 0
    end

    def achieve(name, *args)
      check # TODO: overhead?
      achievement = find_achievement(name, args)
      achievement.call(args)
    end

    def start_call(achievement)
      @indent += 1
      @history[achievement.name] ||= History::Entry.new(self, achievement)
    end

    def end_call(_achievement)
      @indent -= 1
    end

    def indent
      # return "(-1)" if @indent < 0
      "  " * @indent
    end

    private

    def check
      return unless config.sanity_check?

      logger = Unravel.logger

      registry.detect_problems.each do |name, data|
        logger.warn "#{name}: #{data.inspect}" unless data.empty?
      end
    end

    def find_achievement(name, args)
      registry.achievements[name].tap do |achievement|
        arity = achievement.arity
        if arity >= 0 && arity != args.size
          raise ArgumentError, "expected #{arity} args for #{name.inspect}, got: #{args.inspect}"
        end
      end
    rescue Registry::SaneMap::Error::NoSuchKey
      raise HumanInterventionNeeded, "Undefined achievement #{name.inspect}"
    end
  end
end
