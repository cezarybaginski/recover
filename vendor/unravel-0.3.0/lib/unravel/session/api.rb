# TODO: store achievement name as alternative to block

module Unravel
  class Session
    module Api
      def achievement(name, error_map, &block)
        registry.achievements[name] = Achievement.new(self, name, error_map, &block)
      end

      def error
        registry.error_matcher_for
      end

      def root_cause_for(mapping)
        symptom, root_cause = *mapping.first
        registry.root_cause_for[symptom] = root_cause
      end

      # TODO: move logic to registry
      # Usage 1: called with hash: root_cause => achievement_name
      # Usage 3: called with just root_cause, achievement_name
      # TODO: deprecate in favor of new fix syntax
      def fix_for(cause_or_hash, achievement_name = nil)
        raise 'block no longer supported!' if block_given?

        data =
          if cause_or_hash.is_a?(Hash)
            raise ArgumentError, "Expected single-entry hash, got #{cause_or_hash.inspect}" if cause_or_hash.size > 1
            cause_or_hash.to_a.first
          else
            [cause_or_hash, achievement_name]
          end

        cause_name, achievement_name = *data
        raise "bad cause: #{data.inspect} (#{cause_or_hash.inspect})" if cause_name.is_a?(Array)

        achievement_name ||= "fix_#{cause_name}"

        raise HumanInterventionNeeded, "fix for root cause #{cause_name} already exists" if registry.fix_for.key?(cause_name)

        registry.fix_for[cause_name] = achievement_name
      end

      # Shorthand for easy-to-fix and name problems
      # Args
      # symptom, regexp, fix_name, error_contexts, block
      # symptom, regexp, fix_name, error_contexts
      # symptom, regexp, fix_name, block
      # {symptom, regexp}, fix_name, block
      # symptom, regexp, fix_name
      def quickfix(*args, &block)
        full_args = extract_quickfix_args(args)
        hash, fix_name, error_context = *full_args
        # TODO: make sure any given error context matches

        # NOTE: fix_name can be a block

        hash.each_pair do |error_name, pattern|
          root_cause_name = "unapplied_#{fix_name}".to_sym
          error[error_name] = pattern
          root_cause_for(error_name => root_cause_name)
          unless registry.fix_for.key?(root_cause_name)
            registry.fix_for[root_cause_name] = fix_name
          end
        end

        return block_to_achievement(fix_name, error_context, &block) if block_given?

        unless registry.achievements.key?(fix_name)
          begin
            blk = method(fix_name)
            return block_to_achievement(fix_name, error_context, &blk)
          rescue NameError
            return
          end
        end

        existing_context = registry.achievements[fix_name].error_context

        return if existing_context == error_context

        msg = "Cannot use achievement %s in quickfix because it's already defined with different error contexts: %s vs existing %s"
        formatted = format(msg, fix_name.inspect, error_context.inspect, existing_context.inspect)
        raise HumanInterventionNeeded, formatted
      end

      def extract_quickfix_args(args)
        # TODO: clean up this method
        case args.size
        when 4
          symptom, regexp, fix_name, error_contexts = *args
          [{ symptom => regexp }, fix_name, error_contexts]
        when 3
          if args[2].is_a?(Hash)
            hash, fix_name, error_contexts = *args
            [hash, fix_name, error_contexts]
          else
            symptom, regexp, fix_name = args
            [{ symptom => regexp }, fix_name, {}]
          end
        when 2
          if args.first.is_a?(Hash)
            hash, fix_name = *args
            [hash, fix_name, {}]
          else
            symptom, regexp = *args
            [{ symptom => regexp }, "block_fixing_#{symptom}", {}]
          end
        else
          raise ArgumentError, "wrong number of arguments (#{args.size} for 2..4)"
        end
      end

      private

      def block_to_achievement(fix_name, error_context, &block)
        achievement(fix_name, error_context, &block)
      rescue Unravel::Registry::SaneMap::Error::KeyExists
        raise HumanInterventionNeeded, "Cannot use #{fix_name.inspect} as a fix for #{error_context.keys.inspect} because the fix already exists. (Instead, use the Hash form of quickfix())"
      end
    end
  end
end
