module Unravel
  class Session
    class DefaultConfig
      def max_retries
        5
      end

      def after_achievement_success(name)
      end

      def sanity_check?
        true
      end
    end
  end
end
