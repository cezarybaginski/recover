module Unravel
  class Achievement
    # This is used internally for flow control
    class FixableError < RuntimeError
      attr_reader :symptom
      attr_reader :extracted_info
      def initialize(symptom_name, extracted_info)
        @symptom = symptom_name

        # TODO: clean this up
        matchdata = extracted_info
        @extracted_info = matchdata.captures.empty? ? [] : [matchdata]
      end

      def unique_cause_identifier
        [@symptom, @extracted_info]
      end
    end

    class Error < RuntimeError
      class BadReturnValue < NotImplementedError
        attr_reader :name
        attr_reader :result
        def initialize(name, result)
          @name = name
          @result = result
          super "#{name}() unexpectedly returned #{result.inspect} (expected true or exception)"
        end
      end
    end

    class ContextMap < Registry::SaneMap
      def initialize(values)
        super()
        values.each_pair do |ikey, ivalue|
          self[ikey] = Array(ivalue)
        end
      end

      def inspect
        @values.inspect
      end

      def ==(other)
        context_other = other.is_a?(ContextMap) ? other : ContextMap.new(other)
        return false unless keys == context_other.keys
        return false unless values == context_other.values
        true
      end
    end

    attr_reader :name
    attr_reader :session
    attr_reader :error_context

    def initialize(session, name, error_context, &block)
      @session = session
      @name = name
      @error_context = ContextMap.new(error_context)
      @block = block
    end

    def arity
      @block.arity
    end

    def call(args)
      state = session.start_call(self)

      begin
        state.log_start

        res = attempt_to_achieve(state, args)

        raise Error::BadReturnValue.new(name, res) unless res == true

        state.log_done

        session.config.after_achievement_success(name)
        return true

      rescue FixableError => error
        state.log_failure(error)

        cause = session.registry.root_cause_for[error.symptom]

        raise NoKnownRootCause, "Can't find root cause for: #{error.symptom}, #{error.message}" unless cause

        state.log_cause(cause)

        # Since causes can be generic (parametized), they're unique based on error matchers
        unique_context = error.unique_cause_identifier

        state.assure_cause_has_not_occured(cause, unique_context)

        fix_name = fix_for_root_cause(cause)

        state.log_fix(fix_name)

        session.achieve(fix_name, *error.extracted_info)

        state.assure_retires_available

        retry
      end
    ensure
      state.end_call
      session.end_call(self)
    end

    private

    # Use thread so when people call 'return' in achieve blocks, things
    # don't get messed up
    def return_wrap(*args, &block)
      # TODO: enforce using lambdas instead
      if ENV['DETECT_RETURN_IN_BLOCKS'] =~ /true|1/
        Thread.new do
          block.yield(*args)
        end.join
      else
        block.yield(*args)
      end
    rescue LocalJumpError => ex
      ex.exit_value
    end

    def fix!(name, error)
      regexp = session.registry.error_matcher_for[name]

      # TODO: encoding not tested
      match = regexp.match(error.force_encoding(Encoding::ASCII_8BIT))

      # TODO: put fixable error in this class here?
      raise FixableError.new(name, match) if match
      Unravel.logger.debug("     -> Error message does not match: #{name.inspect}\n")
    rescue Registry::SaneMap::Error::NoSuchKey
      Unravel.logger.debug("Available error contexts: #{session.registry.error_matcher_for.keys.inspect}")
      raise HumanInterventionNeeded, "Undefined error context: #{name.inspect} for #{error.inspect}"
    end

    def fix_for_root_cause(cause)
      session.registry.fix_for[cause]
    rescue Registry::SaneMap::Error::NoSuchKey
      raise HumanInterventionNeeded, "No fix for: #{cause}"
    end

    def attempt_to_achieve(state, args)
      exception_types = error_context.keys

      begin
        return return_wrap(*args, &@block)
      rescue *exception_types
        ex = $ERROR_INFO

        STDERR.puts "MATCHING: #{$ERROR_INFO.message.inspect}"

        classes = error_context.keys
        classes.each do |klass|
          next unless ex.class.ancestors.include?(klass)
          matching_errors = Array(error_context[klass])
          matching_errors.each do |error_name|
            fix!(error_name, $ERROR_INFO.message)
          end
        end
        raise
      rescue RuntimeError => ex
        state.log_unknown_exception(ex)
        raise
      end
    end
  end
end
