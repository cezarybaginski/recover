require 'unravel/registry/sane_map'

RSpec.describe Unravel::Registry::SaneMap do
  describe '#[]' do
    context 'when no such key exists' do
      it 'fails with key error' do
        expect { subject[:bad_key] }.to raise_error(described_class::Error::NoSuchKey)
      end
    end
  end

  describe '#[]=' do
    context 'when key already exists' do
      it 'fails with key error' do
        subject[:key] = :foobar
        expect { subject[:key] = :bar }.to raise_error(described_class::Error::KeyExists)
      end
    end
  end
end
