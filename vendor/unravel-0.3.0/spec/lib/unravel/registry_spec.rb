require 'unravel/registry'

RSpec.describe Unravel::Registry do
end

RSpec.describe Unravel::Registry::Achievements do
  let(:session) { class_double('Unravel::Session', new: nil) }

  describe '#map' do
    it 'collects values yielding a block' do
      subject[:foo] = Unravel::Achievement.new(session.new, 'foo', {}) {}
      subject[:bar] = Unravel::Achievement.new(session.new, 'bar', {}) {}
      expect(subject.map { |k| "my_#{k.name}" }).to eq(%w(my_foo my_bar))
    end
  end
end
