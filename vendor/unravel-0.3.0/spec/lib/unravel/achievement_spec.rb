require 'unravel'

RSpec.describe Unravel::Achievement do
  let(:session) { instance_double(Unravel::Session) }
  let(:achievement_name) { :some_achievement }
  let(:error_map) { {} }
  let(:block) { proc {} }

  let(:arguments) { [session, achievement_name, error_map] }

  subject do
    described_class.new(*arguments, &block)
  end

  describe '#initialize' do
    context 'with 3 arguments' do
      it 'works' do
        subject
      end
    end
  end

  describe '#error_context' do
    context 'with a context' do
      let(:error_map) { { RuntimeError => :foo } }
      it 'returns the error context' do
        expect(subject.error_context).to eq(RuntimeError => :foo)
      end
    end

    context 'with an existing valid value' do
      let(:error_map) { { RuntimeError => :foobar } }

      context 'when accessing missing error handler' do
        it 'should fail' do
          expect do
            subject.error_context[NotImplementedError]
          end.to raise_error(Unravel::Registry::SaneMap::Error::NoSuchKey)
        end
      end

      it 'returns item values as arrays' do
        expect(subject.error_context[RuntimeError]).to eq([:foobar])
      end
    end

    context 'with a non-array item value' do
      let(:error_map) { { RuntimeError => :foo } }
      it 'returns item values as arrays' do
        expect(subject.error_context[RuntimeError]).to eq([:foo])
      end

      it 'should be equal with another' do
        expect(subject.error_context).to eq(RuntimeError => :foo)
      end
    end

    context 'with an array item value' do
      let(:error_map) { { RuntimeError => [:foo] } }
      it 'returns item values as arrays' do
        expect(subject.error_context[RuntimeError]).to eq([:foo])
      end

      it 'should be equal with another' do
        expect(subject.error_context).to eq(RuntimeError => :foo)
      end
    end
  end
end
