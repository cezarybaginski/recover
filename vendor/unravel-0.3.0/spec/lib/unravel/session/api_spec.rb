require 'unravel'
require 'unravel/session'

require 'unravel/registry'

RSpec.describe Unravel::Session::Api do
  let(:logger) { instance_double(Logger) }
  let(:config) { Unravel::Session::DefaultConfig.new }

  before do
    allow(Unravel).to receive(:logger).and_return(logger)
    allow(logger).to receive(:debug)
    allow(logger).to receive(:info)
  end

  describe 'integration' do
    describe '#quickfix' do
      context 'with hash arg fix_for' do
        it 'works' do
          expect do
            Class.new(Unravel::Session) do
              def initialize(config)
                @ok = false
                super(config)

                quickfix :some_error, /error!/, :fix_cause

                # TODO: new syntax
                # fix(:recognized_cause, with: :fix_cause)
                achievement(:run, RuntimeError => :some_error) { @ok || raise('error!') }
                achievement(:fix_cause, {}) { @ok = true }
              end
            end.new(config).achieve(:run)
          end.to_not raise_error
        end
      end
    end

    describe '#fix_for' do
      context 'with hash arg fix_for' do
        it 'works' do
          expect do
            Class.new(Unravel::Session) do
              def initialize(config)
                @ok = false
                super(config)

                error[:some_error] = /error!/
                root_cause_for some_error: :recognized_cause
                fix_for recognized_cause: :fix_cause

                # TODO: new syntax
                # fix(:recognized_cause, with: :fix_cause)
                achievement(:run, RuntimeError => :some_error) { @ok || raise('error!') }
                achievement(:fix_cause, {}) { @ok = true }
              end
            end.new(config).achieve(:run)
          end.to_not raise_error
        end
      end

      context 'with standard arguments' do
        it 'works' do
          expect do
            Class.new(Unravel::Session) do
              def initialize(config)
                @ok = false
                super(config)

                error[:some_error] = /error!/
                root_cause_for some_error: :recognized_cause
                fix_for :recognized_cause, :fix_cause

                # TODO: new syntax
                # fix(:recognized_cause, with: :fix_cause)
                achievement(:run, RuntimeError => :some_error) { @ok || raise('error!') }
                achievement(:fix_cause, {}) { @ok = true }
              end
            end.new(config).achieve(:run)
          end.to_not raise_error
        end
      end

      context 'with a block' do
        it 'fails' do
          expect do
            Class.new(Unravel::Session) do
              def initialize(config)
                super(config)

                error[:some_error] = /error!/
                root_cause_for some_error: :recognized_cause
                fix_for(:recognized_cause) { @ok = true }
              end
            end.new(config).achieve(:run)
          end.to raise_error('block no longer supported!')
        end
      end
    end
  end
end
