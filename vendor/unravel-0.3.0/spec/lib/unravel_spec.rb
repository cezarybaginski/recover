require 'unravel'

RSpec.describe Unravel do
  let(:logger) { instance_double(Logger) }

  describe '.logger' do
    around do |example|
      old_logger = Unravel.logger
      example.run
      Unravel.logger = old_logger
    end

    it 'uses given logger' do
      Unravel.logger = logger
      expect(logger).to receive(:info).with('foo')
      Unravel.logger.info('foo')
    end

    context 'when default' do
      before do
        allow(Logger).to receive(:new).with(STDOUT).and_return(logger)
        allow(logger).to receive(:level=)
        allow(logger).to receive(:formatter=)
      end

      it 'uses a shorted default format' do
        the_proc = nil
        expect(logger).to receive(:formatter=) do |formatter_proc|
          the_proc = formatter_proc
        end

        Unravel.logger = nil
        Unravel.logger

        expect(the_proc).to be
        expect(the_proc.call('INFO', Time.now, 'app', 'my message')).to eq("INFO: my message\n")
      end
    end
  end
end

module RoofPainter
  class Config < Unravel::Session::DefaultConfig
    def initialize
      @on_roof = false
      @paint = false
      @ladder = false
      @painted = false
    end

    def paint!
      raise 'already painted!' if @painted
      raise 'not on roof!' unless on_roof?
      raise 'no paint!' unless paint?
      @painted = true
    end

    def climb!
      raise 'no ladder!' unless ladder?
      raise 'already on roof!' if on_roof?
      @on_roof = true
    end

    def buy_paint!
      raise 'already has paint!' if paint?
      @paint = true
    end

    def buy_ladder!
      raise 'ladder already bough!' if @ladder
      @ladder = true
    end

    def paint?
      @paint
    end

    def on_roof?
      @on_roof
    end

    def ladder?
      @ladder
    end
  end

  class VerbosePainter < Unravel::Session
    def initialize(config)
      super(config)

      achievement :paint, RuntimeError => [:not_on_roof, :no_paint] do
        config.paint!
        true
      end

      error[:not_on_roof] = /not on roof!/
      root_cause_for not_on_roof: :did_not_climb
      fix_for did_not_climb: :climb

      achievement :climb, RuntimeError => [:no_ladder] do
        config.climb!
        true
      end

      error[:no_paint] = /no paint!/
      root_cause_for no_paint: :did_not_buy_paint
      fix_for did_not_buy_paint: :buy_paint

      achievement :buy_paint, {} do
        config.buy_paint!
        true
      end

      error[:no_ladder] = /no ladder!/
      root_cause_for no_ladder: :did_not_buy_ladder
      fix_for did_not_buy_ladder: :buy_ladder

      achievement :buy_ladder, {} do
        config.buy_ladder!
        true
      end
    end
  end

  class TersePainter < Unravel::Session
    def initialize(config)
      super(config)

      achievement :paint, RuntimeError => %i(not_on_roof no_paint) do
        config.paint! && true
      end

      quickfix :not_on_roof, /not on roof!/, :climb, RuntimeError => [:no_ladder]
      quickfix :no_paint, /no paint!/, :buy_paint, {}
      quickfix :no_ladder, /no ladder!/, :buy_ladder, {}
    end

    private

    def climb
      config.climb! && true
    end

    def buy_paint
      config.buy_paint! && true
    end

    def buy_ladder
      config.buy_ladder! && true
    end
  end
end

RSpec.describe 'Unravel examples' do
  let(:logger) { instance_double(Logger) }
  before do
    allow(Unravel).to receive(:logger).and_return(logger)
    allow(logger).to receive(:debug)
    allow(logger).to receive(:info)
  end

  let(:config) { RoofPainter::Config.new }
  context 'with convenience API' do
    it 'works' do
      RoofPainter::VerbosePainter.new(config).achieve(:paint)
    end
  end

  context 'without convenience API' do
    it 'works' do
      RoofPainter::TersePainter.new(config).achieve(:paint)
    end
  end

  context 'with no paint' do
    context 'with a ladder' do
      before { config.buy_ladder! }

      context 'when already on roof' do
        before { config.climb! }

        it 'works' do
          RoofPainter::TersePainter.new(config).achieve(:paint)
        end
      end
    end
  end

  context 'when calling non-existing achievement' do
    subject do
      Class.new(Unravel::Session) do
        def initialize(config)
          super(config)
          achievement(:run, {}) { achieve :foobar }
        end
      end.new(config)
    end

    before { allow(logger).to receive(:error) }

    it 'fails with HumanInterventionNeeded error and logs the error' do
      expect(logger).to receive(:error).with(/Achieving :run failed: unrecognized exception: #<Unravel::HumanInterventionNeeded: Undefined achievement :foobar>/)
      expect { subject.achieve(:run) }.to raise_error(Unravel::HumanInterventionNeeded, 'Undefined achievement :foobar')
    end
  end

  context 'without defined error context' do
    subject do
      Class.new(Unravel::Session) do
        def initialize(config)
          super(config)

          achievement :run, RuntimeError => [:unknown_error_context] do
            raise 'Some Error'
          end
        end
      end.new(config)
    end

    before { allow(logger).to receive(:warn) }

    it 'fails with HumanInterventionNeeded error and logs the error' do
      expect(logger).to receive(:warn).with('Unknown contexts: [:unknown_error_context]')
      expect { subject.achieve(:run) }.to raise_error(Unravel::HumanInterventionNeeded, 'Undefined error context: :unknown_error_context for "Some Error"')
    end
  end

  context 'with simple quickfix' do
    context 'with method' do
      it 'works' do
        expect do
          Class.new(Unravel::Session) do
            def initialize(config)
              @ok = false
              super(config)

              achievement :run, RuntimeError => [:unknown_error_context] do
                raise 'Some Error' unless @ok
                true
              end

              quickfix :unknown_error_context, /Some Error/, :my_method
            end

            def my_method
              @ok = true
              true
            end
          end.new(config).achieve(:run)
        end.to_not raise_error
      end

      context 'with same reoccuring problem' do
        it 'fails with a useful error' do
          expect do
            Class.new(Unravel::Session) do
              def initialize(config)
                @ok = false
                super(config)

                achievement :run, RuntimeError => [:unknown_error_context] do
                  raise 'Some Error' unless @ok
                end

                quickfix :unknown_error_context, /Some Error/, :my_method
              end

              def my_method
                true
              end
            end.new(config).achieve(:run)
          end.to raise_error(Unravel::SameCauseReoccurringCause, "Cannot achieve :run because root cause \"unapplied_my_method\" for error [:unknown_error_context, []] still isn't fixed (the root causes's symptoms reoccurred)")
        end
      end

      context 'with 2 quickfixes leading to same fix' do
        it 'fails with useful error' do
          expect do
            Class.new(Unravel::Session) do
              def initialize(config)
                @ok = false
                super(config)

                achievement :run, RuntimeError => [:unknown_error_context, :unknown_error_context2] {}

                quickfix :unknown_error_context, /Some Error1/, :my_method
                quickfix :unknown_error_context_2, /Some Error2/, :my_method
              end

              def my_method
              end
            end.new(config)
          end.to_not raise_error
        end

        context 'with different error contexts' do
          it 'fails with useful error' do
            expect do
              Class.new(Unravel::Session) do
                def initialize(config)
                  @ok = false
                  super(config)

                  achievement :run, RuntimeError => [:unknown_error_context, :unknown_error_context2] {}

                  quickfix :unknown_error_context, /Some Error1/, :my_method, RuntimeError => :bar
                  quickfix :unknown_error_context_2, /Some Error2/, :my_method
                end

                def my_method
                end
              end.new(config)
            end.to raise_error(Unravel::HumanInterventionNeeded, "Cannot use achievement :my_method in quickfix because it's already defined with different error contexts: {} vs existing {RuntimeError=>[:bar]}")
          end
        end
      end

      # TODO: refactor changes by calling methods externally
      context 'with Hash form quickfixes' do
        it 'works' do
          expect do
            Class.new(Unravel::Session) do
              def initialize(config)
                @ok = false
                @ok2 = false
                super(config)

                achievement :run, RuntimeError => [:unknown_error_context1, :unknown_error_context2] do
                  raise 'Some Error1' unless @ok
                  raise 'Some Error2' unless @ok2
                  true
                end

                quickfix({
                           unknown_error_context1: /Some Error1/,
                           unknown_error_context2: /Some Error2/
                         }, :my_method)
              end

              def my_method
                @ok ? @ok2 = true : @ok = true
                true
              end
            end.new(config).achieve(:run)
          end.to_not raise_error
        end
      end

      context 'with irrelevantly differrent error messages' do
        subject do
          Class.new(Unravel::Session) do
            def initialize(config)
              @calls = 0
              super(config)

              achievement :run, RuntimeError => [:unknown_error_context1, :unknown_error_context2] do
                raise "[pid:#{1000 + @calls}] Some Error1"
              end

              quickfix({ unknown_error_context1: /Some Error1/ }, :my_method)
            end

            def my_method
              raise 'Called more than once!' if @calls > 0
              @calls += 1
              true
            end
          end.new(config)
        end

        before { allow(logger).to receive(:warn) }

        it 'fails with error about root cause not fixed' do
          expect { subject.achieve(:run) }.to raise_error(Unravel::SameCauseReoccurringCause, "Cannot achieve :run because root cause \"unapplied_my_method\" for error [:unknown_error_context1, []] still isn't fixed (the root causes's symptoms reoccurred)")
        end
      end

      context 'when args include error context' do
        it 'works' do
          expect do
            Class.new(Unravel::Session) do
              def initialize(config)
                @ok = false
                super(config)

                achievement :run, RuntimeError => [:unknown_error_context] do
                  raise 'Some Error' unless @ok
                  true
                end

                quickfix({ unknown_error_context: /Some Error/ }, :my_method, {})
              end

              def my_method
                @ok = true
                true
              end
            end.new(config).achieve(:run)
          end.to_not raise_error
        end
      end

      context 'with block and name conflicting with existing achievement' do
        it 'fails with useful error' do
          expect do
            Class.new(Unravel::Session) do
              def initialize(config)
                @ok = false
                super(config)

                achievement :run, RuntimeError => [:unknown_error_context, :unknown_error_context2] {}

                quickfix :unknown_error_context, /Some Error1/, :my_method
                quickfix :unknown_error_context_2, /Some Error2/, :my_method do
                end
              end

              def my_method
              end
            end.new(config)
          end.to raise_error(Unravel::HumanInterventionNeeded, 'Cannot use :my_method as a fix for [] because the fix already exists. (Instead, use the Hash form of quickfix())')
        end
      end
    end

    context 'with too many args' do
      it 'works' do
        expect do
          Class.new(Unravel::Session) do
            def initialize(config)
              super(config)

              achievement :run, RuntimeError => [:unknown_error_context] {}
              quickfix :unknown_error_context, /Some Error/, :my_method, {}, :foobar
            end
          end.new(config).achieve(:run)
        end.to raise_error(ArgumentError, 'wrong number of arguments (5 for 2..4)')
      end
    end

    context 'without method' do
      subject do
        Class.new(Unravel::Session) do
          def initialize(config)
            super(config)

            achievement :run, RuntimeError => [:unknown_error_context] do
              raise 'Some Error'
            end

            quickfix :unknown_error_context, /Some Error/, :my_method
          end
        end.new(config)
      end

      it 'fails with HumanInterventionNeeded error' do
        expect { subject.achieve(:run) }.to raise_error(Unravel::HumanInterventionNeeded, /Undefined achievement :my_method/)
      end
    end

    context 'when exception is derived' do
      it 'fails with HumanInterventionNeeded error' do
        expect do
          Class.new(Unravel::Session) do
            def initialize(config)
              @ok = false
              super(config)

              achievement :run, StandardError => [:unknown_error_context] do
                raise Errno::ENOENT, 'Some Error' unless @ok
                true
              end

              quickfix :unknown_error_context, /Some Error/, :my_method
            end

            def my_method
              raise 'already ok!' if @ok
              @ok = true
              true
            end
          end.new(config).achieve(:run)
        end.to_not raise_error
      end
    end

    context 'when exception type is not handled' do
      it 'fails with HumanInterventionNeeded error' do
        expect do
          Class.new(Unravel::Session) do
            def initialize(config)
              @ok = false
              super(config)

              achievement :run, RuntimeError => [:unknown_error_context] do
                raise Errno::ENOENT, 'Some Error' unless @ok
              end

              quickfix :unknown_error_context, /Some Error/, :my_method
            end

            def my_method
            end
          end.new(config).achieve(:run)
        end.to raise_error(Errno::ENOENT)
      end
    end

    context 'when there is no matching fix' do
      it 'fails with HumanInterventionNeeded error' do
        expect do
          Class.new(Unravel::Session) do
            def initialize(config)
              @ok = false
              super(config)

              achievement :run, Errno::ENOENT => [:unknown_error_context] do
                raise Errno::ENOENT, 'foobar' unless @ok
              end

              quickfix :unknown_error_context, /Some Error/, :my_method
            end

            def my_method
            end
          end.new(config).achieve(:run)
        end.to raise_error(Errno::ENOENT)
      end
    end

    context 'with a block instead of achievement' do
      it 'fails with HumanInterventionNeeded error' do
        expect do
          Class.new(Unravel::Session) do
            def initialize(config)
              @ok = false
              super(config)

              achievement :run, Errno::ENOENT => [:an_error] do
                raise Errno::ENOENT, 'Some Error' unless @ok
                true
              end

              quickfix :an_error, /Some Error/ do
                raise 'already ok!' if @ok
                @ok = true
                true
              end
            end
          end.new(config).achieve(:run)
        end.to_not raise_error
      end
    end

    context 'when block is given too many arguments' do
      it 'fails with HumanInterventionNeeded error' do
        expect do
          Class.new(Unravel::Session) do
            def initialize(config)
              @ok = false
              super(config)

              achievement :run, {} { |_abc| }
            end
          end.new(config).achieve(:run, :foo, :bar)
        end.to raise_error(ArgumentError, 'expected 1 args for :run, got: [:foo, :bar]')
      end
    end

    context 'with too many attempts to fix' do
      it 'works' do
        expect do
          Class.new(Unravel::Session) do
            def initialize(config)
              @calls = 0
              super(config)

              achievement :run, RuntimeError => [:unknown_error_context1] do
                raise "[pid:#{1000 + @calls}] Some Error1"
              end

              quickfix({ unknown_error_context1: /\[pid:(\d\d\d\d)\] Some Error1/ }, :my_method)
            end

            def my_method(_pid)
              @calls += 1
              true
            end
          end.new(config).achieve(:run)
        end.to raise_error(Unravel::OutOfAttempts, 'Achieving :run took too many attempts. Verify that errors are correctly matched and that the fixes are deterministic')
      end
    end

    context 'with only 1 attempt available and failing' do
      let(:config) do
        Class.new(Unravel::Session::DefaultConfig) do
          def max_retries
            1
          end
        end.new
      end

      it 'fails only attempting once' do
        expect do
          Class.new(Unravel::Session) do
            def initialize(config)
              @calls = 0
              super(config)

              achievement :run, RuntimeError => [:unknown_error_context1] do
                raise "[pid:#{1000 + @calls}] Some Error1" if @calls == 0
              end

              quickfix({ unknown_error_context1: // }, :my_method)
            end

            def my_method
              @calls += 1
              true
            end
          end.new(config).achieve(:run)
        end.to raise_error(Unravel::OutOfAttempts, 'Achieving :run took too many attempts. Verify that errors are correctly matched and that the fixes are deterministic')
      end
    end

    context 'bugfix' do
      before do
        allow(logger).to receive(:error)
      end

      subject do
        Class.new(Unravel::Session) do
          def setup
            achievement(:all, Errno::ENOENT => :no_x) { raise Errno::ENOENT, 'X' }

            quickfix(:no_x, /X/, :create_git_dir,
                     Unravel::HumanInterventionNeeded => [:no_y]) do
              raise Unravel::HumanInterventionNeeded, 'Y'
            end

            quickfix({ no_y: /Y/ }, :fix_it)
          end

          def fix_it
            raise 'OK'
          end
        end.new.tap(&:setup)
      end

      it 'fails correctly and logs error' do
        expect(logger).to receive(:error).with(/Achieving :fix_it failed: unrecognized exception: #<RuntimeError: OK>/)
        expect { subject.achieve(:all) }.to raise_error(RuntimeError, 'OK')
      end
    end

    context 'when achievement does not return true' do
      it 'fails with HumanInterventionNeeded error' do
        expect do
          Class.new(Unravel::Session) do
            def initialize(config)
              super(config)
              achievement :run, {} { :baz }
            end
          end.new(config).achieve(:run)
        end.to raise_error(NotImplementedError, 'run() unexpectedly returned :baz (expected true or exception)')
      end
    end

    context 'when the fix is missing' do
      before do
        allow(logger).to receive(:warn)
      end
      it 'fails with HumanInterventionNeeded error' do
        expect do
          Class.new(Unravel::Session) do
            def initialize(config)
              super(config)
              achievement(:run, RuntimeError => :some_error) { raise 'Some error' }
              error[:some_error] = /Some error/
              root_cause_for some_error: :undefined_fix
            end
          end.new(config).achieve(:run)
        end.to raise_error(Unravel::HumanInterventionNeeded, 'No fix for: undefined_fix')
      end
    end
  end
end
