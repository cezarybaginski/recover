class workstation::heroku {
  apt::key { 'heroku':
    id         => '150C6249147592DE6D91981CC927EBE00F1B0520' ,
    source =>  'https://cli-assets.heroku.com/apt/release.key',
  }

  apt::source { 'heroku':
    location => 'https://cli-assets.heroku.com/branches/stable/apt',
    release => '',
    repos    => './',
    require  =>  Apt::Key['heroku']
  }
}
