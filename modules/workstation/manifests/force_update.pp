class workstation::force_update {
  exec { 'my_apt_update':
    command => '/usr/bin/apt-get -y update',
    user => 'root',
    timeout => 0,
    logoutput => true,
    require => Exec['apt-get-fix'],
  }
}
