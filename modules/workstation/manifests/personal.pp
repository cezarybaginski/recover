class workstation::personal {
  package { [
    'feh',
    'workrave',
    'urlscan',
    'anki',
    'mutt',
    'gnucash',
    'chromium-browser',
    'elinks',
    'fluxbox',
    'doconce',
    'pandoc',
    'texlive-lang-polish'
  ]: ensure => latest }
}
