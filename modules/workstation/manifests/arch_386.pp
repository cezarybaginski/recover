class workstation::arch_386 {
  exec { '386_arch':
    command => '/usr/bin/dpkg --add-architecture i386',
    user => 'root',
    timeout => 0,
  }
}
