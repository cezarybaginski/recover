class workstation::clang_dev {
  apt::key { 'clang': id     => '6084F3CF814B57C1CF12EFD515CF4D18AF4F7421' }
  apt::source { 'clang':
    location => "http://apt.llvm.org/${lsbdistcodename}",
    release  =>  "llvm-toolchain-${lsbdistcodename}",
    require  => Apt::Key['clang']
  }
}
