class workstation::webkit_dev {
  package { [
    'qt5-default',
    'libqtwebkit-dev',
    #'gstreamer-tools',
  ]: ensure => latest }
}
