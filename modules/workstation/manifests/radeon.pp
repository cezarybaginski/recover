class workstation::radeon {
  include arch_386

  apt::key { 'oibaf': id                           => '5ABCE68FF4633EA42E219156957D2708A03A4626' }
  apt::ppa { 'ppa:oibaf/graphics-drivers': require => Apt::Key['oibaf'] }
  apt::ppa { 'ppa:oibaf/gallium-nine': require     => Apt::Key['oibaf'] }
  apt::key { 'commendsarnex': id                   => '7E705155A1631A9469F85A9B67B66C2B82BF0DF4' }
  apt::ppa { 'ppa:commendsarnex/winedri3': require => Apt::Key['commendsarnex'] }

  exec { 'apt-get-fix':
    command => "/usr/bin/apt-get -f -y install",
    unless  => "/usr/bin/apt-get check",
    require => [
      Apt::Ppa['ppa:commendsarnex/winedri3'],
      Apt::Ppa['ppa:oibaf/graphics-drivers'],
      Apt::Ppa['ppa:oibaf/gallium-nine'],
      Exec['386_arch'],
    ]
  }

  package {
    [ 'mesa-vdpau-drivers', ]:
      ensure => latest,
      require => Exec['apt_update']
  }
}
