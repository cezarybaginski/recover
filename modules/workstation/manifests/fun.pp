class workstation::fun {
  package { [
    'playonlinux',
    'geeqie',
    'comix',
  ]: ensure => latest }
}
