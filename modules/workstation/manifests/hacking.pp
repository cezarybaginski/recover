class workstation::hacking {
  $essential_packages = [
    'vim',
    'git',
    'tmux',
    'ack-grep',
    'symlinks',
    'coreutils',
    'openssh-server',
  ]

  # essentials
  package { $essential_packages : ensure => latest }
}
