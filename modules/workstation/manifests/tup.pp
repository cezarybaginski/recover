class workstation::tup {
  apt::key { 'cezary': id                          => '24A821C81A84DC0AF034CFEED17917E87B82B7C3' }
  apt::ppa { 'ppa:cezary0/tup': require            => Apt::Key['cezary']}

  package { 'tup': ensure  => latest, require      => [Apt::Ppa['ppa:cezary0/tup'], Exec['apt_update']] }
}
