class workstation::crypto {
  package { [
    'cryptsetup',
    'cryptsetup-bin',
    'keyutils',
    #'john',
    ]: ensure => latest
  }
}
