class workstation::wine_dev {
  apt::key { 'wine': id                            => '883E8688397576B6C509DF495A9A06AEF9CB8DB0' }
  apt::ppa { 'ppa:ubuntu-wine/ppa': require        => Apt::Key['wine'] }
  apt::ppa { 'ppa:wine/wine-builds': require       => Apt::Key['wine'] }
}
