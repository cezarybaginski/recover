#class ubuntu_extras {
#  apt::source { "archive.ubuntu.com-${lsbdistcodename}":
#    location => 'http://archive.ubuntu.com/ubuntu',
#    key      => '630239CC130E1A7FD81A27B140976EAF437D05B5',
#    repos    => 'main universe multiverse restricted',
#  }

#  apt::source { "archive.ubuntu.com-${lsbdistcodename}-security":
#    location => 'http://archive.ubuntu.com/ubuntu',
#    key      => '630239CC130E1A7FD81A27B140976EAF437D05B5',
#    repos    => 'main universe multiverse restricted',
#    release  => "${lsbdistcodename}-security"
#  }

#  apt::source { "archive.ubuntu.com-${lsbdistcodename}-updates":
#    location => 'http://archive.ubuntu.com/ubuntu',
#    key      => '630239CC130E1A7FD81A27B140976EAF437D05B5',
#    repos    => 'main universe multiverse restricted',
#    release  => "${lsbdistcodename}-updates"
#  }

#  apt::source { "archive.ubuntu.com-${lsbdistcodename}-backports":
#    location => 'http://archive.ubuntu.com/ubuntu',
#    key      => '630239CC130E1A7FD81A27B140976EAF437D05B5',
#    repos    => 'main universe multiverse restricted',
#    release  => "${lsbdistcodename}-backports"
#  }
#}

class workstation::all {
  # include ubuntu_extras
  include workstation::dev_tools
  include workstation::audio
  include workstation::fun
  include workstation::personal
  include workstation::compression
  include workstation::browsers
  include workstation::rails_dev
  include workstation::webkit_dev
  include workstation::crypto
  include workstation::heroku

  # TODO: only needed on docker to avoid apt failures
  package { ['udev']: ensure => latest, require => Exec['apt_update']; }

  ->

  package { [
    'gnome-session',
    'synaptic',
    'amd64-microcode',
    'dnsmasq',
    'x11-xserver-utils',
    # 'bzr-rewrite', # bad name
    # 'xlockmore-gl', # bad name
    # 'skype', # bad repo
    # 'pdfedit', # bad name
    # 'hex', # bad name
    # 'google-chrome-stable', # bad repo
    # 'acroread', # bad repo
    # 'heroku', # bad repo
    # 'avidemux', # bad repo
    # 'google-chrome', # bad repo
    #'uncrustify',
    #'vagrant',
    #'adequate',
    #'alacarte',
    #'anthy',
    #'aqemu',
    #'banshee',
    #'bittornado',
    #'langdrill',
    #'istanbul',
    #'inkscape',
    #'cheese',
    #'scribus',
    # 'zoom',
  ]:
    ensure => latest,
    require => Exec['apt_update']
  }

  include workstation::force_update
  include workstation::arch_386

  package { 'gnome-tweak-tool': ensure => latest, require => Exec['apt_update'] }

  #apt::key { 'noobslab': id =>'4FA44A478284A18C1BA4A9CAD530E028F59EAE4D' }
  #->
  #apt::ppa { 'ppa:noobslab/themes': }
  #->
  #package { [ 'delorean-dark', 'arc-theme' ]: ensure  => latest, require => Exec['my_apt_update'] }

  # On xenial, arc-theme has moved to:
  # deb http://download.opensuse.org/repositories/home:/Horst3180/xUbuntu_16.04/ /

# ppa:ravefinity-project/ppa ambiance-blackout-colors
# ppa:noobslab/themes arc-theme

  include workstation::tup
  include workstation::clang_dev
  include workstation::wine_dev

  exec { 'apt-get-fix':
    command => "/usr/bin/apt-get -f -y install",
    unless  => "/usr/bin/apt-get check",
    require => [
      Apt::Ppa['ppa:ubuntu-wine/ppa'],
      Apt::Ppa['ppa:wine/wine-builds'],
      Apt::Ppa['ppa:cezary0/tup'],
      Apt::Source['clang'],
      Apt::Source['heroku'],
      Exec['386_arch'],
    ]
  }

  package { [
    #'wine',
    #'wine-staging',
    #'wine-staging-i386',
    #'wine-staging-amd64',
    #'wine1.9-i386',
    #'wine1.9-amd64',
    #'wine1.8-i386',
    #'wine1.8-amd64',
    'libomxil-bellagio0',
    #'winehq-staging',
    'fonts-droid-fallback',
    'ttf-mscorefonts-installer',
    'winetricks',
    'wine-gecko',
    #'winehq-devel',
    'wine-mono'
  ]: 
    ensure => latest,
    require => [ Exec['my_apt_update'], ]
  }
}

