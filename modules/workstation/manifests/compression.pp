class workstation::compression {
  package { [
    'unrar',
    'p7zip-full',
    'cabextract',
  ]: ensure => latest }
}
