class gpg (){ }

define gpg::import_and_trust_pub(
  $homedir = '',
  $gpg_key_id = '',
  $gpg_file = $title,
){
  include gpg
}

define gpg::import_priv(
  $owner = $title,
  $homedir = '',
  $gpg_key_id = '',
  $content = '',
  $gpg_file = "${homedir}/${gpg_key_id}.priv"
){
  include gpg
  exec { "gpg setup ":
    user => $owner,
    command => "/usr/bin/gpg --homedir ${homedir} --list-keys",
    unless => "/usr/bin/test -d ${homedir}",
  } ->
  file { $gpg_file:
    ensure => file,
    owner => $owner,
    mode => '640',
    content => $content
  } ->
  exec { "gpg import restore privkey":
    # import + automating gnupg "manual" key trust crap (still better than "trustmodel always")
    command => "/usr/bin/gpg --homedir ${homedir} --batch --yes --import ${gpg_file}",
    unless => "/usr/bin/gpg --homedir ${homedir} --list-key ${gpg_key_id}",
    user => $owner
  }
}
