# s3cmd - represents Amazon IAM credentials file for a bucket
define s3cmd::iam_file(
  $file = $title,
  $user = '',
  $group = '',
  $access_key = '',
  $secret_key = '',
  $bucket_location = 'US'
) {
  package { 's3cmd':
    ensure => 'present',
  }


  #TODO: use AWS_CREDENTIAL_FILE for s3 config?
  #TODO: template file?
  file { $file:
    ensure  => file,
    content => "[default]
access_key = ${access_key}
secret_key = ${secret_key}
bucket_location = ${bucket_location}
",
    group   => $group,
    owner   => $user,
    mode    => '0640'
  }
}
