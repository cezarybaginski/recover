class system_tools {
  $system_tools = [
    'gparted',
    'lvm2',
    'ntp',
    'ntpdate',
    'lm-sensors',
    'mbr',
    'dar',
    'apt-btrfs-snapshot',
    'eatmydata',
    'apt-cacher-ng',
    'baobab',
    'bleachbit',
    'btrfs-tools',
    'wireshark',
    'wicd-gtk',
    'wicd',
    'htop',
    'fancontrol',
    'at',
    'iotop',
    'strace',
  ]
  package { $system_tools: ensure => latest; }
}

class virtualization {
  include system_tools

  $me = lookup('my_login')
  class { 'docker':
    docker_users => [$me],
    manage_kernel => false, # interferes with lowlatency kernel
  }

  #apt::key { 'docker': id => '36A1D7869245C8950F966E92D8576A8BA88D21E9' }
#class { 'docker':
#}

  #package { [
  #  # 'docker-engine',
  #  #'aufs-tools',
  #]: ensure => latest; }

  package { [
    'virtualbox',
    'virtualbox-guest-additions-iso',
    'qemu-kvm',
    'qemulator',
  ]: ensure => latest, }
}
