class workstation::low_mem {
  package { 'zram-config': ensure => latest; }
}
