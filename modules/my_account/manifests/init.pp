class my_account {
  $me = lookup('my_login')
  $my_home  = lookup("my_home")
  $admin = lookup('admin_group')

  group { $admin: ensure => present }
  group { $me: ensure => present  }

  package { 'zsh' : ensure => latest }

  user { $me:
    ensure     => present,
    managehome => true,
    shell      => '/usr/bin/zsh',
    groups     => [$me, $admin],
    require    => [
      Group[$me],
      Group[$admin],
      Package['zsh']
    ],
  }

  rvm::system_user { $me:
    create => false,
    require => User[$me]
  }

  file { $my_home:
    ensure  => directory,
    owner   => $me,
    group   => $me,
    require => [User[$me], Group[$me]],
  }
}
