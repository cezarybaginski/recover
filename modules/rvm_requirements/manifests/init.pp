class rvm_requirements {
  $rvm_packages = [
    'g++',
    'libreadline6-dev',
    'libreadline-dev',
    'zlib1g-dev',
    'libssl-dev',
    'libyaml-dev',
    'libsqlite3-dev',
    'sqlite3',
    'autoconf',
    'libgdbm-dev',
    'libncurses5-dev',
    'automake',
    #'libtool', # disabled temporarily
    'libtool-bin',
    'bison',
    'gawk',
    'patch',
    'bzip2',
    'curl',
    'pkg-config',
    'libffi-dev',
  ]

  package { $rvm_packages : ensure => latest }
}
