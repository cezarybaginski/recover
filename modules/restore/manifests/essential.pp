class restore::essential {
  include workstation::compression

  package { [
    'iputils-ping',
    's3cmd',
    'awscli',
    'ack',
    'vim',
  ]: ensure => latest }

  #include multiuser_rvm
  include my_account

  $me = lookup('my_login')
  $admin = lookup('admin_group')
  $my_home  = lookup("my_home")
  $my_workspace = "$my_home/workspace"
  $my_workstation_checkout = "${my_workspace}/czarek_workstation"
  $my_workstation_settings_file = "${my_workstation_checkout}/settings.mk"
  $my_workspace_settings = lookup('my_workspace_settings')
  $my_workstation_url = lookup('my_workspace_repo_url')

  host { 'gem_proxy':
    ip => lookup('gem_proxy.ip'),
    host_aliases => lookup('gem_proxy.name')
  }

  file { $my_workspace:
    ensure  => directory,
    owner   => $me,
    group   => $me,
    require => Class['my_account'],
  }

  vcsrepo { $my_workstation_checkout:
    ensure   => latest,
    force    => false,
    user     => $me,
    owner    => $me,
    group    => $me,
    provider => git,
    source   => $my_workstation_url,
    revision => 'master',
    require  => [File[$my_workspace] ],
  }

  file { $my_workstation_settings_file :
    ensure  => present,
    owner   => $me,
    group   => $me,
    content => $my_workspace_settings,
    require => [
      Vcsrepo[$my_workstation_checkout],
      Host['gem_proxy']
    ]
  }

  exec {'update_workstation':
    environment => ["HOME=$my_home"],
    command => "/bin/bash -c '${my_workstation_checkout}/update_from_puppet'",
    cwd     => $my_workstation_checkout,
    user    => $me,
    group   => $me,
    timeout => 0,
    require => [
      File[$my_workstation_settings_file]
      #, Class['multiuser_rvm']
    ]
  }
}
