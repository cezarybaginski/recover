# sanity check for Facter
case $::default_gateway {
  '', undef: {
    fail('$::default_gateway is not available - FACTERLIB not set?')
  }
}

class { 'apt':
  update => {
    frequency => 'daily',
  },
}

class system_packages {
  include ::apt
  include workstation::hacking
  include rvm_requirements
}

# Nodes
node default {
  include system_packages
}

# Vagrant recovery test box
node database {
  if $::virtual == 'virtualbox' {
    include vagrant
  }
  include system_packages
}

# My workstation
node /^czarek-m85m-us2h(.hi.link)?$/ {
  include system_packages
  include workstation
  if ! str2bool($::is_docker) {
    include virtualization
    include low_mem
  }
}

# My workstation
node /^cezary.linuxonmac$/ {
  include system_packages
  include workstation
  include virtualization
}

# Copy of workstation (linux2)
node 'cezary-pc' {
  include system_packages
  include workstation
  include workstation::radeon
  include virtualization
  include low_mem
}

# Docker recovery test box
node /^[a-f0-9]{12}(\.home$)?$/ {
  include ::apt
  #  include system_packages
  #include workstation
  include restore::essential
}

# vi: ft=puppet :
