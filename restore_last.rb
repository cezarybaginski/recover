#!/usr/bin/env ruby
# vi: ft=ruby ts=2 sw=2 et:

require 'pathname'
require 'time'
require 'digest'
require 'shellwords'
require 'fileutils'
require 'open3'
require 'yaml'

# TODO: wrap in module

def system!(*args)
  return true if system(*args)
  raise "command failed (#{$?.exitstatus}): #{args.join(' ')}"
end

def system?(*args)
  system(*args).tap do |result|
    raise "command failed: #{args.join(' ')}" if result.nil?
  end
end

module MyBackups
  class AwsS3Entry
    class Error < RuntimeError
    end

    attr_reader :size
    attr_reader :filename
    attr_reader :timestamp

    def initialize(output_line)
      date, time, size, filename = parse_aws_output_entry(output_line)
      raise Error, "bad input" if [date, time, size, filename].any?(&:empty?)

      timestamp = Time.parse("#{date} #{time}")
      @size = size
      @filename = filename.freeze
      @timestamp = timestamp.freeze
    end

    private

    def parse_aws_output_entry(entry)
      # e.g.
      #2018-11-25 04:03:00 1303708785 foo.gpg

      entry.split(' ', 4).freeze
      # [ date, time, size, name ]
    end
  end

  class AwsS3Listing
    def initialize(raw_listing)
      @listing = raw_listing
    end

    def each_file(&block)
      @listing.lines.each do |raw_line|
        line = raw_line.chomp
        next if directory?(line)
        yield AwsS3Entry.new(line)
      end
    end

    private

    def directory?(line)
      # e.g. "              PRE .md5/"
      # e.g. "2018-11-25 03:59:39          0"
      !!/
      ^\s+PRE\s\/$
      |
      ^ \d{4}-\d{2}-\d{2} \s \d{2}:\d{2}:\d{2} \s+ \d+ \s $
      /x.match(line)
    end
  end

  class Config
    class Error < RuntimeError
    end

    def self.decrypt(data:, passphrase_file:)
      args = %w(gpg -d --pinentry-mode=loopback --passphrase-file)

      output, errors, status = Open3.capture3(*args, passphrase_file, stdin_data: data)
      return output if status.success?

      STDERR.puts errors
      raise Error, "Bad exitcode #{status.exitstatus} from running #{args.inspect}"
    end

    attr_reader :data
    def initialize(yml)
      @data = YAML.load(yml)
      @configuration = {
        my_private_key: -> { data.fetch(:private_key) },
        my_private_key_passphrase_file: -> { f = data.fetch(:passphrase_file); return f if File.exist?(f); ENV.fetch('RESTORE_PASSPHRASE_FILE') },
        my_private_key_id: -> { data.fetch(:private_key_id) },
        bucket_name: -> { Pathname.new(data.fetch(:s3_bucket_name)) },
        bucket_path: -> { Pathname.new(data.fetch(:s3_bucket_path)) },
        my_btrfs_backup_regexp: -> { Regexp.new(data.fetch(:snapshot_regexp)) },
        aws_credentials: -> { data.fetch(:aws_credentials) },

        # new data
        username: -> { data.fetch(:username) },
        groupname: -> { data.fetch(:groupname) },
        uid: -> { data.fetch(:uid)},
        gid: -> { data.fetch(:gid)},
        usergroups: -> { data.fetch(:usergroups) },
        root_image_size: -> { data.fetch(:my_root_image_size) },

        last_inc_volname: -> { last_inc.to_s[Regexp.new(data.fetch(:inc_volname_regexp)), :volname] },
        regexp_for_latest_inc: -> { Regexp.new(format(data.fetch(:inc_regexp_format), Regexp.quote(base_timestamp))) },

        homedir: -> { "/home/#{username}" },

        my_backup_file?: ->(filename) { my_btrfs_backup_regexp.match(filename) },
        my_backup_file_inc?: ->(filename) { regexp_for_latest_inc.match(filename) },

        base_timestamp: -> { last_base.to_s[my_btrfs_backup_regexp, :timestamp] },

        root_image: -> { '/root/restore_btrfs.img' },
        tmp_image: ->  { "#{root_image}.fallocate_tmp" },
        receive_mountpoint: -> { '/root/restore_btrfs' },

        aws_cache_path_format: -> { "aws_ls_cache.%<path_hash>s" },
        bucket_full_path: -> { bucket_name + bucket_path },
        last_base: -> { @last_base ||= last_aws_entry { |entry| my_backup_file?(entry.filename) } },
        last_inc: -> { @last_inc ||= last_aws_entry { |entry| my_backup_file_inc?(entry.filename) } },
        aws_s3_file_uri: ->(pathname) { "s3://#{pathname.cleanpath.to_s}" },
        aws_dir_path: ->(path) { path.cleanpath.to_s + "/" },

        aws_cache_filepath: -> {
          path_hash = Digest::SHA256.hexdigest(bucket_full_path.to_s);
          Pathname.new(format(aws_cache_path_format, path_hash: path_hash))
        }
      }
    end

    def method_missing(method, *args)
      entry = @configuration.fetch(method)
      entry.call(*args)
    end

    def aws_recent_backup_ls
      # sample aws s3 ls output:
      #2018-11-25 04:03:00 1303708785 foo.gpg

      args = %w(aws s3 ls) + [aws_dir_path(bucket_full_path)]
      output, errors, status = Open3.capture3(aws_credentials, *args)

      return output if status.success?

      STDERR.puts errors
      raise Error, "Bad exitcode #{status.exitstatus} from running #{args.inspect}"
    end

    def cached_recent_aws_listing
      aws_cache_filepath.binread
    rescue Errno::ENOENT
      aws_recent_backup_ls.tap do |output|
        aws_cache_filepath.binwrite(output)
      end
    end

    def last_aws_entry
      dump = cached_recent_aws_listing

      latest = nil

      listing = AwsS3Listing.new(dump)
      listing.each_file do |entry|
        next unless yield(entry)

        latest = entry if latest.nil? || entry.timestamp > latest.timestamp
      end

      return Pathname.new(latest.filename) if latest

      raise Error, "No recognized backups found among #{listing.size} entries!"
    end

    def batch_gpg_args
      [].tap do |args|
        args << 'gpg' << '--batch' << '--no-tty' << '--yes' << '--no-options'
        args << '--pinentry-mode=loopback'
      end.freeze
    end

    def import_key
      args = batch_gpg_args.dup
      args << '--passphrase-file' << my_private_key_passphrase_file
      args << '--import' << '-'

      key_data = my_private_key

      Open3.popen2e(*args) do |i, oe, t|
        i.write(key_data)
        i.close

        oe.each do |line|
          puts "  > #{line}"
        end

        exit_status = t.value
        return if exit_status.success?

        raise "GPG import failed (#{exit_status})!"
      end
    end
  end

  class Snapshot
    attr_reader :config
    attr_reader :type
    attr_reader :imagefile
    attr_reader :name
    attr_reader :receive_mountpoint
    def initialize(config, imagefile, type, name, receive_mountpoint)
      @config = config
      @type = type
      @imagefile = imagefile
      @name = name
      @receive_mountpoint = receive_mountpoint
    end

    def provisioned?
      File.exist?(checkfile)
    end

    def checkfile
      "#{imagefile}.#{type}_done"
    end

    def download_cache
      File.join(File.dirname(imagefile), "current_#{type}.btrfs")
    end

    def download_partial
      "#{download_cache}.partial"
    end

    def safe(args)
      args.map(&:shellescape).join(' ')
    end

    def debug(message)
      STDERR.puts(message)
    end

    def provision!
      if File.exist?(download_cache)
        debug "Btrfs receiving #{name}"
        system!('btrfs', 'receive', '-f', download_cache, receive_mountpoint)
        return FileUtils.touch(checkfile)
      end

      bucket_path = config.bucket_full_path + name
      aws_args = ['aws','s3', 'cp', '--quiet', config.aws_s3_file_uri(bucket_path), '/dev/stdout']

      config.import_key unless system?('gpg', '--list-secret-keys', config.my_private_key_id)

      gpg_args = config.batch_gpg_args.dup << '-d' << '--passphrase-file' << config.my_private_key_passphrase_file

      debug "Btrfs receiving #{name} directly from AWS"
      env = config.aws_credentials
      system!(env, "#{safe(aws_args)} | #{safe(gpg_args)} | tee #{download_partial.shellescape} | btrfs receive -f /dev/stdin #{receive_mountpoint.shellescape}")

      FileUtils.mv(download_partial, download_cache)
      FileUtils.touch(checkfile)
    end
  end

  class Restorer
    def initialize(config)
      @config = config
    end

    def has_mount?(dir)
      system?('findmnt', dir)
    end

    def prepare_image
      return if File.exist?(config.root_image)

      system!('fallocate', '-l', config.root_image_size, config.tmp_image)
      system!('mkfs.btrfs', config.tmp_image)
      FileUtils.mv(config.tmp_image, config.root_image)
      return if File.exist?(config.root_image)

      raise "failed to prepare destination fs image"
    end

    def prepare_mountpoint
      prepare_image
      return if has_mount?(config.receive_mountpoint)

      system!('mount','-o', 'X-mount.mkdir=0700', config.root_image, config.receive_mountpoint)
      return if has_mount?(config.receive_mountpoint)

      raise "Failed to mount for receive"
    end

    def snapshot_provisioned_root_image
      prepare_mountpoint
      snap = Snapshot.new(config, config.root_image, "snap", config.last_base, config.receive_mountpoint)
      return if snap.provisioned?

      snap.provision!
    end

    def valid_provisioned_root_image
      snapshot_provisioned_root_image
      inc = Snapshot.new(config, config.root_image, "inc", config.last_inc, config.receive_mountpoint)
      return config.root_image if inc.provisioned?

      inc.provision!
      config.root_image
    end

    require 'etc'

    def has_group?(name, gid)
      Etc.getgrnam(name).gid == gid
    rescue ArgumentError
      false
    end

    def create_group
      return if has_group?(config.groupname, config.gid)
      system!('groupadd', '-g', config.gid.to_s, config.groupname)
      return if has_group?(config.groupname, config.gid)
      raise "Failed to create user group"
    end

    def create_user
      Etc.getpwnam(config.username)
    rescue ArgumentError
      create_group
      system!('useradd', '-u', config.uid.to_s, '-g', config.groupname, '-m', '-G', config.usergroups.join(','), config.username)
      Etc.getpwnam(config.username)
    end

    def valid_homedir
      return config.homedir if Dir.exist?(config.homedir)
      create_user
      return config.homedir if Dir.exist?(config.homedir)
      raise "Failed to create user and/or home dir"
    end

    def restore
      # TODO: mount last subvolume
      subvol = config.last_inc_volname
      has_mount?(config.homedir) || system!('mount', '-o', "subvol=#{subvol.shellescape}", valid_provisioned_root_image, valid_homedir)
      system!('ls', '-lrt', config.homedir)
    end

    private

    attr_reader :config
  end
end

# How to encrypt:
# 1. create config.yml (use unencrypted attachment here as an example)
# 2. `GPG_TTY=$(tty) gpg -a --pinentry-mode=loopback --s2k-count 65011712 --passphrase-file password.txt -c config.yml`
# 3. Put it after the __END__ marker

yml = MyBackups::Config.decrypt(data: DATA, passphrase_file: ENV.fetch('RESTORE_PASSPHRASE_FILE'))
IO.binwrite('config.yml', yml)

config = MyBackups::Config.new(yml)
restorer = MyBackups::Restorer.new(config)
restorer.restore

__END__

-----BEGIN PGP MESSAGE-----

jA0ECQMCiYJNCjCfW9H/0usBYNEfUujPEz0hBrLxF5wLSS5lFaV+6+BD/dJMkEcS
kyjJTCMbgj13esMiFQmj98bvhIPIEb+AuGJXnGv6v7jDE/6PwYqil5ddVclD491T
KDdkB+tQY98SOHkGOKXPQSSe+62lO/RkyXw6zn0qszrpsWj13P0U42PMHxOnJEXr
8rsKSteiZIXKlJTH47A2/OpZ4YMA0FX+RBD8nrVdEFZNSyZue3Yo0DwRTp/MNmzW
E+h8ROdGERqNjgfVptplYoF/EGYxsDixERtddQH6iLijcCaXduB9o2EDS84b3uF6
LhqWuMO4n1bpy2NtUZaw8AR1ujbe4AykkikIwYq/60Unp5vXiXKOiyqfJ7bNRAbQ
FWN7tvQv5S8a9RaI1w2mko18nnYs3Ey0AeB11JzXFOkYeu2h2s+lfoBSMFj6YAh8
39TuiREbG5wGulpi3RZlKOiY/hFpBwFXjHrb8SFrf9XQBtbvQDPCATcbawqb6Un5
0B45/o50KxaW7sr1ZP1Nu6qphLB/Rax0Awt6bFFwTvFVCHoR7eKjv36YcO8Qkkyv
ZAjdaHNucajKavsi36VTD2XVTuPvwQZwuxsO5QMphLCl8RA1GF5R/4wK//g/9GQD
L91SW34WYFIYATHDf3LdDvpmGw8a2PZmJK5Awv+Pz5SZkHJK4cGZEY8AMNRnTEH7
S01NmJOHCb9w5LCDB4JCfWpV6rXEwXA7PZENJL/3oqYNTIxg/G9S44+ZzJZugGRh
KSzdtS46FDo62bq0FRVJUyvMqt2sAHYbanrbtuRq1NMd8LQJw30neh4MS7miDq7d
2TCRb2gyQNdLdx7/QHdeJp9xRd8IYopMbu1LvL66avcQeS8dAxCsSpFzWCve2THG
LYXJxJH5AH7cLf4j77LZ9oSvGfQV7zZ1ks8tnfV8Kk/nAO0GDyqNdESCpmZct7sI
ZyZsB5wwfJ3dCl9YkqFpinSi8DfCWFjU1PqzYcuoB2jjqgBZEb4H6Rd2CtPwFaN6
L2GqqDOOHLhrJMULpX+tkyu7n78ZOQjwgNVvQm4qpJx9Ni/5IF5Ur4deIo06aAO6
OdLvICfLOieTTjnZ8hSsUfRpKP35392nYyy5yiirxTGs3hMoUIgwqYF4EfZpqhnR
1s7P5+eDRBqBoNwgAEX95aD1R7tt0rVmCV14U2SLFOLKPWXTY9hbSdcvJ2gA3Wk0
zc1g6en3kAz6UmqJS0vYuG0e+2AZE68BQmvgao2crn8mcSFIIrrGCZyiJ74OD/86
Qr4qzvQ5oDMQ4SLgV6rp+bJD6g6kDB7Vk3duHqOnAqwgLfx9hBvY4mIHTpsHKSK3
BwfXKxtyFFD+2lfDya7SnbCIEAIMfo5POt+mMEPSO8AwEOAK1CmJV1lh/CVPOtO+
49aywBW2RUDTA224j+MWAK/RNIWPT1rW9RbVjcCmBi1ziZ4/Wyh4iVgzrceNjiI1
wwE7tjuIkRPSIk6AkhEbaTTQS0rtkqO8pVq6qIwIrrFIszhRl6ug1v57DA728yT2
KQJXiuTBqFD/AgTl57XIR2jHHwOb4Rdagdw1xTQE8rfT2RZs/qiaohJR9gmuoC05
3K9dWgqY41ztkI22pCX09QHxxhxgH7LX9pHR8b+NGUVtcHE5jcsKIiugAGld5B97
by+M3sXSNN8XijFFTpX8k0ap2HrpFw8XT10y7icMTbTxuOR1okiV21TSO0LhHFdO
fqb+P8rpOS8Dl1F+iyVsHptZ94wvw7hf6GjvjSiPXQBiLkEQFEtuQp9Zl4MSkL47
VFJYXukiotAdiPR2ZKCMXJL8eJC3ZYtItZwvMMhsJojueR6bjAMK+l42A3wwDEv7
+x658ONq9ude4GnzttRY1JWtb7RlED/LMqPetmZUIi6LLxIWcgeuJwhz9KCQRc9F
a+YOAvi1sa3bua04hoqcO4Lv4RLT92bs7pLBgswCkgSPSP09CKvvXvkmhjH05Tzo
80uzz1FUdLi4+gQiRsX/igZrFMs8K8xvWmgxlA/jjV5wwrCx6uoMZeCtNz6fzpvp
80m5jOETN69sf4v6VZTOu1WMr1anNvJ8k5S3A80NvHKtCQMa0WJ2VPjhFVMLotiG
WVaGihmlV4Aweg+0aT1eBzVWZhD9IDQvvGnGfsYW0ZxOTEZj1eO3yrQnv5db/xe4
Pt37xncJ+vK11GsdHDoEtXkNhKtiyRUhyKA3lK1poPXOg/Odle6FqMeOKisMvyci
c951XywPkqd8bf6Ko/n7wVmeWOt+jWEtW4yvjcnVYjUXum0x0NbVU5GCzMx4WLxq
jm48Pzd4DDeY7twDDCUKpXEjdh5HdSFA6oCxaeOmAuW1RCmSat3linhNjmFW1WPn
e6iUtn/gFJJHM+q1ahoguUE6OkW78pCm+eCpfPSj9WMZ3pGckzMJ/dAfvBY8HZyc
CvRah6fTrIVpovF28BBY7H2X2gKFN70C/QnT3rHPekwBR68ETT+AoRoJJCBOhDDe
xBx7+ho1YY+0/G3PjaiQM9InlPcxJDkVH4w8W5tSSKLhagcmHM2jElHYlZNuZgdm
3/BSVFmNHtb7C6M5wvUixzsU2sLoKSr2aizH3eRiyXYMZhs/w7MM9vnOtmqSuUOo
uM0JN6JeVfdO7XzTpEBbv9jJS2h0KyDbK8Imr856D4TpBOgHIY02SnNA6xnHV+fU
dlL+AoeZYFYnPgKA5uwebu7WcCM/84gpn02wA1z2QfJLTMIFAMIx71w2S7ePzTM5
qkpFz49YR2bNvoioQ8U+2Pqky50B3jXI95ysBILJkKCSaJgSym3QFBTj1K4Ruix4
LIluw2H36CMM0on4SZfx7pgPqbsGz8frCjM63w==
=fHmR
-----END PGP MESSAGE-----
