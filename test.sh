#!/bin/sh

set -eu

#sudo which ruby
#sudo gem install bundler

#FACTERLIB=/home/czarek/workspace/recover/custom_facts bundle exec puppet parser validate manifests/site.pp


sudo gem list -i -q deep_merge || sudo bundle update

sudo /usr/bin/env FACTERLIB=/home/czarek/workspace/recover/custom_facts bundle exec puppet apply --config ./puppet.conf --modulepath=modules --hiera_config=hiera.yaml --debug --test --noop manifests/site.pp "$@"
