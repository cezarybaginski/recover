#!/usr/bin/env ruby

require 'logger'

$:.unshift File.join(Dir.pwd, 'vendor/unravel-0.3.0/lib')
require 'unravel'
require 'unravel/exec'

require_relative 'workstation/provisioner'
require_relative 'workstation/results_file'
require_relative 'workstation/standard_error_messages'

def debug?
  (ENV['RESTORE_DEBUG'] || "") =~ /1|true/
end

STDERR.puts "DEBUG MODE ACTIVE (RESTORE_DEBUG is true)" if debug?

Unravel.logger.level = debug? ? Logger::DEBUG : Logger::INFO

begin
  Workstation::Provisioner.new(Workstation::Provisioner::Config.new).provision
rescue Unravel::HumanInterventionNeeded => e
  abort "ABORT: #{e.class.to_s}: #{e.message.to_s}:\n #{e.backtrace * "\n -- "}"
end
