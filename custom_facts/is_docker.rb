Facter.add('is_docker') do
  setcode do
    begin
      File.read('/proc/1/cgroup').match(/docker/) ? 'true' : 'false'
    rescue Errno::ENOENT
      'false'
    end
  end
end
