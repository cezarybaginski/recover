#!/bin/sh
set -eu
set -o pipefail

NEEDED="bzip2 curl git gpg ruby ruby-bundler"

PACKAGES=""
for k in $NEEDED; do
  which $k || PACKAGES="$PACKAGES $k"
done

if [ -n "$PACKAGES" ]; then
  apt-get -y update && apt-get -y install $PACKAGES
fi

# Needed on Ubuntu 18 even - to avoid path failures with bundler
# gem update --system

if [ -z "${RECOVER_SETUP_ONLY:-}" ]; then
  export GPG_TTY=$(tty)

  seed=recovery_seed.gpg

  [ -e "$seed" ] || curl --retry 3 -sSL -o "$seed" "https://cezarybaginski.s3.amazonaws.com/backup/$seed"

  gpg --batch --homedir /tmp --pinentry-mode loopback --no-default-keyring --no-options --no-tty --yes --passphrase-file /dev/shm/restore_secrets/recovery_seed_passphrase -d  $seed > runme.sh

  cat runme.sh |sed -e 's/set -o pipefail/\0\n\nexport GPG_TTY=$(tty)\n/; s/--no-use-agent/--no-tty --pinentry-mode loopback /g; s/gpg --import/\0 --no-tty --batch --pinentry-mode loopback --passphrase-file "$GPG_PASSPHRASE_FILE"/' | sed -e 's/git clone/\0 --depth 1/'  > runme2.sh

  chmod +x runme2.sh
  bash ./runme2.sh
fi
