require 'digest/sha1'

module Workstation
  class ResultsFile

    class Error < RuntimeError
      class Outdated < Error
        attr_reader :filename
        def initialize(filename)
          @filename = filename
        end

        def to_s
          "Manifest was updated since last run [previous hash in: #{filename}]"
        end
      end
    end

    attr_reader :filename

    def initialize
      @filename = 'last_run.sha1'
      @script_file = 'manifests/site.pp'
    end

    def check!
      last_run = IO.read(filename)
      fail Error::Outdated, filename unless last_run == current_digest
    rescue Errno::ENOENT
      raise Error::Outdated, filename
    end

    def mark_successful
      IO.write(filename, current_digest)
    end

    private

    def current_digest
      Digest::SHA1.file(@script_file).hexdigest
    end
  end
end
