require 'pathname'
require 'open3'
require 'shellwords'

require_relative 'provisioner/config'

module Workstation
  class Provisioner < Unravel::Session
    BINARY_TO_PACKAGE_MAPPING = {
      # different
      'compiler' => 'gcc',
      'rvm_requirements_installed' => 'gawk',
      'rubygems_installed' => 'rubygems',
      'mkmf' => 'ruby-dev', # required for building rvm ruby
      'symlinks' => 'symlinks', # require for old workspace symlinking scripts
      'fc-cache' => 'fontconfig', # required after powerline fonts install
      'lsbdistcodename' => 'lsb-release',
      'add-apt-repository' => 'software-properties-common',

      # same
      'curl' => 'curl',
      'make' => 'make',
      'gcc' => 'gcc',
      'git' => 'git',
      'lsb' => 'lsb-release',
      'software' => 'software-properties-common',
    }


    # Not a real exception - just to break up provisioning into steps
    # which the parent can resume
    class Progress < RuntimeError
    end

    def initialize(config)
      super(config)

      achievement :provisioned, {} do
        achieve :user_locale_available
        unless config.database?
          achieve :user_config_files
          achieve :valid_rvm_ruby_detected
          achieve :workstation_cache_dirs
          achieve :puppet_clean
        end
        true
      end

      achievement :user_locale_available, Unravel::Exec::Error::Standard => :missing_locale do
        # Check local
        check_locale_cmd = "locale -a | grep ja_JP.utf8 || { echo 'locale ja_JP.utf8 not installed' >&2; /bin/false; }"
        Unravel::Exec %W(su -l -c #{check_locale_cmd})
      end

      achievement :user_config_files, Unravel::Exec::Error::Standard => :no_user_config_files do
        Unravel::Exec %w(/bin/ls -l /home/czarek/.zshrc)
      end

      achievement :valid_rvm_ruby_detected, { Unravel::Exec::Error::Standard => :invalid_rvm_ruby_version } do
        # detect Ruby > 2.2.0
        check_ruby_cmd = "ruby -e '\"a\".unicode_normalize'|| { echo 'invalid RVM Ruby version' >&2; /bin/false; }"
        Unravel::Exec %W(su -l -c #{check_ruby_cmd} czarek)
      end

      achievement :workstation_cache_dirs, Unravel::Exec::Error::Standard => :no_cached_dirs do
        # last thing set up - by workstation scripts running puppet (cache_dirs)
        Unravel::Exec %W(/bin/ls -l /home/czarek/junk)
      end

      achievement :puppet_clean, {ResultsFile::Error::Outdated => :manifest_newer_than_applied} do
        ResultsFile.new.check!
        true
      end

      achievement :packages,
        Unravel::Exec::Error::Standard =>[
          :no_ack_grep,
          # :no_delorean_dark_error,
          :no_psql,
      ], Unravel::Exec::Error::ENOENT => [ :no_psql, :no_ack_grep ] do
        # Unravel::Exec 'dpkg -s delorean-dark >/dev/null'
        Unravel::Exec %w(/usr/bin/ack --version)
      end

      achievement(:apt_updated, {}) { Unravel::Exec %w(apt-get -y update) }

      achievement :bundler_installed, Unravel::Exec::Error::Standard => [:no_gem_command] do
        cmd ='gem install bundler --pre && gem contents bundler && bundler --version'
        Unravel::Exec %W(#{cmd})
      end

      quickfix :no_puppet_command, /bundler: command not found: puppet/, :puppet_installed
      achievement :puppet_installed, Unravel::Exec::Error::Standard => [:no_gem_command] do
        Unravel::Exec %w(gem install puppet)
      end

      quickfix :no_gem_command, /fake_command_placeholder_that_never_matches/, :rubygems_installed

      quickfix({
        no_symlinks_command: /(?<binary>[a-z]*) not installed!/, # my error
        no_curl_command: /: command not found: (?<binary>[a-z]*)/,
        make_not_found: /Gem::(?:Ext::BuildError|Installer::ExtensionBuild(?:er)?Error): ERROR: Failed to build gem native extension.*sh: 1: (?<binary>[a-z]*): not found/m,
        no_gcc: /Gem::Ext::BuildError: ERROR: Failed to build gem native extension.*make: (?<binary>[a-z]*): Command not found|The (?<binary>compiler) failed to generate an executable file/m,
        no_lsb_release: /Unable to determine lsbdistid, please install (?<binary>[a-z-]*) first|Evaluation Error: Error while evaluating a Function Call, (?<binary>lsbdistcodename) fact not available:/,
        no_add_apt_repository: %r{Could not find command '/usr/bin/(?<binary>[a-z-]*)'},
        rvm_failed_requirements: /Error running '(?<binary>requirements_debian_update_system)/,
        no_mkmf: /Gem::Installer::ExtensionBuild(?:er)?Error: ERROR: Failed to build gem native extension.*cannot load such file -- (?<binary>[a-z]*) \(LoadError\)/m,
        no_mkmf_h: /Gem::Ext::BuildError: ERROR: Failed to build gem native extension.*(?<binary>mkmf)\.rb can't find header files for ruby at/m,
      }, :achieve_binary_installed)

      quickfix({
        no_bundler_git: /You need to install (?<binary>[a-z]*) to be able to use gems from (?:[a-z]*) repositories/,
        cannot_find_git: /cannot find (?<binary>[a-z]*)/,
      }, :achieve_binary_installed)

      achievement :achieve_binary_installed, Unravel::Exec::Error::Standard => [:apt_fetch_error, :broken_packages] do |match|
        Unravel::Exec %W(apt-get -y install #{package_for(match[:binary])})
      end

      # TODO: fix if RVM multiuser is used ?
      achievement :rubydev_installed, Unravel::Exec::Error::Standard => [:apt_fetch_error, :broken_packages] do
        Unravel::Exec %W(apt-get -y install rubygems)
      end

      achievement :gems_installed,  {
        Unravel::Exec::Error::Standard => [:no_bundler_command, :no_bundler_git],
        # NOTE: match make_not_found first
        Unravel::Exec::Error::Silent => [:no_mkmf, :no_mkmf_h, :make_not_found, :no_gcc]
      } do
        # NOTE: --quiet option silences the root cause (just says gem install failed)
        Unravel::Exec %w(bundle install)
      end

      achievement :retry, {} do
        # Do nothing - the problem occurs only once and fixes itself
      end

      achievement :user_locale_generated, {} do |match|
        Unravel::Exec ["locale-gen", match[:locale]]
      end

      achievement :activate_and_boot_docker, {} do
        Unravel::Exec ['systemctl enable docker && systemctl start docker']
      end

      achievement :puppet_applied, {
        Unravel::Exec::Error::Standard => [
          :apt_outdated,
          :broken_packages,

          :no_bundler_command,
          :no_bundler_gem,
          :no_puppet_command,
          :no_curl_command,
          :no_symlinks_command,
          :no_gem_in_any_source,
          :apt_fetch_error,
          # :no_delorean_dark,
          :no_tup,

          :invalid_resource_vscrepo,
          :unknown_function_bool,
          :no_puppet_class,
          :no_declared_class,

          :rvm_failed_requirements,
          :no_lsb_release,
          :no_add_apt_repository,
          :no_docker_command,
          :cannot_connect_to_docker
        ],
        Errno::ENOENT => [
          :spawn_no_bundle_command,
          :no_bundler_command,
        ]
      } do
        run_puppet_apply
      end

      quickfix({
        invalid_resource_vscrepo: /Invalid resource type vcsrepo/,
        unknown_function_bool: /Error: Unknown function validate_bool at/,
        no_puppet_class: /Error: Evaluation Error: Error while evaluating a Function Call, Could not find class (?<class>[a-z:_]*) for/,
        no_declared_class: /Error: Evaluation Error: Error while evaluating a Resource Statement, Could not find declared class (?<class>[a-z:_]*)/,
      }, :librarian_updated, { Unravel::Exec::Error::Silent => :cannot_find_git }) do |mod|
        Unravel::Exec %w(bundle exec librarian-puppet update)
      end

      quickfix({
        # puppet apply
        no_ack_grep: StandardErrorMessages.ennoent('/usr/bin/ack'),
        no_psql: StandardErrorMessages.ennoent('/usr/bin/psql'),
        no_user_config_files: StandardErrorMessages.ls_cannot_access('/home/czarek/.zshrc'),
        no_cached_dirs: StandardErrorMessages.ls_cannot_access('/home/czarek/junk'),
        invalid_rvm_ruby_version: /invalid RVM Ruby version/,
        manifest_newer_than_applied: /Manifest was updated since last run/,
        no_docker_command: / docker: not found$/,
        #no_delorean_dark_error: /dpkg-query: package 'delorean-dark' is not installed and no information is available/,
      }, :puppet_applied_successfully) do
        achieve :puppet_applied
      end

      quickfix({
        no_bundler_command: StandardErrorMessages.ennoent('bundle'),
        no_bundler_gem: /to_specs': Could not find 'bundler'.*\(Gem::LoadError\)/,
        spawn_no_bundle_command: /`spawn': #{StandardErrorMessages.ennoent('bundle')} \(Errno::ENOENT\)/,
      }, :bundler_installed_successfully) do
        achieve :bundler_installed
      end

      quickfix :missing_locale, /locale (?<locale>[^ ]+) not installed/, :user_locale_generated
      quickfix :cannot_connect_to_docker, /Cannot connect to the Docker daemon\. Is the docker daemon running on this host\?/, :activate_and_boot_docker

      quickfix :no_gem_in_any_source, /Could not find .*-\d+\.\d+\.\d+ in any of the sources/, :gems_installed_successfully do
        achieve :gems_installed
      end

      quickfix({
        no_tup: %r{E: Unable to locate package tup},
        apt_fetch_error: /E: Unable to fetch some archives, maybe run apt-get update or try with --fix-missing\?/m,
        broken_packages: /E: Unable to correct problems, you have held broken packages/,
        #no_delorean_dark: %r{E: Unable to locate package delorean_dark},
        apt_outdated: /Error: Could not update: Execution of '\/usr\/bin\/apt-get -q -y -o DPkg::Options::=--force-confold install .*' returned 100: Reading package lists/
      }, :simple_method_fix_apt_get)

    end

    def provision
      single_achievement = config.single_achievement
      return achieve(single_achievement) if single_achievement

      #Docker.logger = Unravel.logger
      achieve :provisioned
      achieve :packages
    end

    def save_apply_script(filename, args)
      lines = ["#!/bin/sh"]
      env = args.first
      rest = args[1..-1]
      require 'shellwords'
      lines += [env.map { |k,v| "#{k}=#{v}"}.join(" ") + " " + Shellwords.join(rest)]
      Pathname(filename).write(lines.join("\n") + "\n")
    end

    def run_puppet_apply
      env = {
        'LC_ALL' => 'C.UTF-8',

        #'FACTERLIB' => %w(custom_facts).map do |dir|
        #  (Pathname.pwd + dir).to_s
        #end.join(':')
      }

      #      config_data =<<EOF
      #[user]
      #environment = production
      #hiera_config = hiera.yaml
      #strict_variables = true
      #factpath = custom_facts
      #EOF

      args = [env] + %w(bundle exec puppet apply)
      args << '--debug' if config.puppet_debug?
      args += %W(
         --verbose
   --confdir=.
   --codedir=.
   --factpath=custom_facts
   --strict_variables
   --environment=production
         --detailed-exitcodes manifests)

      save_apply_script('run_puppet.sh', args)

      out, error, status = Open3.capture3(*args)

      if [0,2].include? status.exitstatus
        raise Provisioner::Progress.new("progress happened: #{manifest}") if config.steps?
        ResultsFile.new.mark_successful
      else
        STDERR.puts "===========> EXIT CODE: #{status.inspect}"
        Unravel.logger.debug "OUTPUT from #{args.inspect}: -----"
        Unravel.logger.debug "#{out}"
        Unravel.logger.debug "Errors from #{args.inspect}: -----"
        Unravel.logger.debug "#{error}"
        # puppet shows errors as Notice entries
        raise Unravel::Exec::Error::Standard, out + "\n" + error
      end
      true
    end

    class Error < RuntimeError
    end

    def package_for(binary)
      BINARY_TO_PACKAGE_MAPPING.fetch(binary) do
        raise Error, "Package not recognized/allowed: #{binary.inspect}"
      end
    end

    def simple_method_fix_apt_get
      Unravel::Exec ['apt-get install -f && apt-get update']
    end
  end
end
