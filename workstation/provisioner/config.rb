module Workstation
  class Provisioner < Unravel::Session
    class Config  < Unravel::Session::DefaultConfig
      def max_retries
        10
      end

      def after_achievement_s(name)
        raise Provisioner::Progress, "progress happened: #{name}" if steps?
      end

      def steps?
        (ENV['RESTORE_STEPS_ONLY'] || "") =~ /1|true/
      end

      def puppet_debug?
        (ENV['PUPPET_DEBUG'] || "") =~ /1|true/
      end

      def single_achievement
        achieve(ARGV[0].to_sym) if ARGV[0]
      end

      def database?
        `hostname`.chomp == 'database'
      end

      def postgres_version
        '9.5'
      end

      def postgres_var_dir
        "/var/lib/postgresql/#{postgres_version}"
      end
    end
  end
end
