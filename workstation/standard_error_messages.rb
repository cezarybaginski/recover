module Workstation
  class StandardErrorMessages
    def self.ls_cannot_access(path)
      %r{/bin/ls: cannot access '?#{Regexp.quote(path)}'?: No such file or directory}
    end

    def self.ennoent(path)
      /No such file or directory - #{Regexp.quote(path)}/
    end

    def self.ruby_ennoent(path)
      /No such file or directory @ rb_sysopen - #{Regexp.quote(path)}/
    end
  end
end
